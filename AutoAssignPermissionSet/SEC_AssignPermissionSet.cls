public class SEC_AssignPermissionSet {
    
    @future
    public static void assignPermissionSetToChampions(Set<Id> contactIdSet){
        String permissionSetName = 'Permission_Set_Name';
        List<PermissionSetAssignment> insertPermissionSetList = new List<PermissionSetAssignment>();
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> assigneeIdSet = new Set<Id>();
        List<PermissionSet> permissionSetList = [SELECT Id, Name 
                                                            FROM PermissionSet 
                                                            WHERE Name = :permissionSetName];
                                                            
        if(!permissionSetList.isEmpty()){
            String fetchPermissionSetId = permissionSetList[0].Id;
            for(User u : [SELECT Id,ContactId FROM User WHERE ContactId IN :contactIdSet]){
                userIdSet.add(u.Id);
            }
            for(PermissionSetAssignment psa : [SELECT Id,AssigneeId 
                                                FROM PermissionSetAssignment 
                                                WHERE PermissionSetId =:fetchPermissionSetId 
                                                AND AssigneeId IN:userIdSet ]){
                assigneeIdSet.add(psa.AssigneeId);
            }
            for(Id userId : userIdSet){
                if(!assigneeIdSet.contains(userId)){
                    PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = fetchPermissionSetId, AssigneeId = userId);
                    insertPermissionSetList.add(psa);
                }
               
            }
            if(!insertPermissionSetList.isEmpty()){
                insert insertPermissionSetList;
            }
        }
    }
}