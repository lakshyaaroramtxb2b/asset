You can call the checkCustomPermission from lwc components to check whether a particular user has custom permission or not.
For eg,

checkPermission() {
        checkCustomPermission({ permissionAPIName: ["Allow_to_Unlock_Slr_day_night_checks"] })
        .then(result => {
            this.isUnlockTeamMember = result['Allow_to_Unlock_Slr_day_night_checks'];
            if(this.isUnlockTeamMember){
                this.showButton = true;
            }
            else{
                this.showButton = false;
            }
        })
        .catch(error => {
            showAjaxErrorMessage(this, error);
        });
}