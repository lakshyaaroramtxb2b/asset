public class CheckCustomPermission{
    @AuraEnabled
    public static Map<String, Boolean> checkCustomPermission(List<string> permissionAPIName){
        Map<String, Boolean> permissions = new Map<String, Boolean>();
        for( string per : permissionAPIName ){
            permissions.put(per, FeatureManagement.checkPermission(per) );
        }
        return permissions;
    }
}