1. Pass the response from VF page to LWC on verify callback.

var verifyCallback = function (response) {
      var message = {name : 'Unlock',payload : response}
       parent.postMessage(message,"{!$Site.BaseSecureUrl}");
};

Sample response string: 

03AGdBq24RMWIws-TzsayYSg7D6hERdBXLVlNmKRsdeL1VuN_JzKHqE7Oz3RDaqRliCVMtc-WHHhUC0LlXpBqQHGYax3NDaD7OXx9DvE611WJkTHBUYHUI9qdDGSGmcIu8KQ5KCDok1vDv6xkkS6TpI8QBUjr1UTRdLLMcM9nu0tb1Yj1Js7RUqjv45m8RpwsEOgt30tmmmjF7P_jx6k7TPPObp6u9B2JbMQxL4s9M8bVkznxy0suHfjsnOko4WLnP0boADl0qtxU7kQGfc6kA75qIZ8JJtD3FzMT3PvOQzxQBQTq63B-RnGAk2Da3gaq-cxwgOxDo_tqVUyBUNE7Nv7g9Zn7dRL9ij80lyIIUiMrhXFxudTWfVV1yugJvbOytFlu2HQ8ZYOwvrizcj-e-Eu7kt8w2HlZGyPmyggZCQ1hw34cVz8zLMeHRO6ySKNmk6I2vKGpzo3fdPIbKZMyd_qNDsTsbUAwTcQ


2. In LWC, on button submit-> call the method from the Apex class which will validate the response is valid or not. 

3. Setup named credential or remote setting for end point.

4. Copy the apex class from the branch

For test class, refer this trailhead: https://trailhead.salesforce.com/content/learn/modules/apex_integration_services/apex_integration_rest_callouts

