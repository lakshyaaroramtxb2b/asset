public without sharing class RecaptchaService {
    private static String recaptchaSecretKey = 'XXXXXXXXXXXXXXXXXXXXXXXXX';
    @AuraEnabled
    public static String getServerResponse(String recaptchaResponse){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:Google_Recaptcha_End_Point'); //named credential api name
        request.setMethod('POST');
        request.setBody('secret=' + recaptchaSecretKey + '&response=' + recaptchaResponse);
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            if (result.containsKey('success') && result.get('success') == true) {
                if (result.containsKey('score')) {
                    return 'Success - v3';
                } else {
                    return  'Success - v2';
                }
            } 
        }
        return 'Invalid Verification Request';
    }
}