public class SEC_TermsAndConditionController {
    
    // SObject to store contact record details.
    public Security_Champion_Nomination__c contactObj {get; set;}
    
    public SEC_TermsAndConditionController() {
        contactObj = new Security_Champion_Nomination__c();
    }
    
    // Remote Action method that is called from the VF page to fetch lookup field data.
    @RemoteAction
    public static List<customData> getLookupData(String searchTerm){
        String userId = UserInfo.getUserId();
        User u = [SELECT Id,ContactId FROM User WHERE Id =:userId];
        String contactId = u.contactId;
        List<Security_Champion_Nomination__c> recordList = new List<Security_Champion_Nomination__c>();
        Set<String> productTagIdSet = new Set<String>();
        List<customData> dataList = new List<customData>();
        searchTerm= '%'+searchTerm+'%';
        // Dynamic query is created to get the matching records for the lookup field.
        String query  = 'Select Id,Product_Tag__c,Product_Tag_Name__c From Security_Champion_Nomination__c WHERE Product_Tag_Name__c LIKE \'' + searchTerm + '\' AND Name_of_Security_Champion_Nominee__c =:contactId LIMIT 10';
        recordList = Database.query(query);
        if(!recordList.isEmpty()) {
            for(Security_Champion_Nomination__c obj : recordList){
                if(!productTagIdSet.contains(obj.Product_Tag__c)){
                    productTagIdSet.add(obj.Product_Tag__c);
                    dataList.add(new customData(obj.Product_Tag_Name__c, obj.Product_Tag__c ));
                }
            }
            return dataList;
        } 
        else {
            return null;
        }    
    }
    
    @RemoteAction
    public static PageReference FinishLoginFlowHome() {
        String userId = UserInfo.getUserId();
        User u = [SELECT Id,ContactId FROM User WHERE Id =:userId];
        List<Contact> contactList = new List<Contact>([SELECT Id,Attested__c FROM Contact WHERE Id =:u.ContactId]);
        contactList[0].Attested__c = true;
        if(!Test.isrunningTest())
            update contactList;
        return Auth.SessionManagement.finishLoginFlow();
    }
    
    public static PageReference checkAndRedirect(){
        String userId = UserInfo.getUserId();
        User u = [SELECT Id,ContactId FROM User WHERE Id =:userId];
        List<Security_Champion_Nomination__c> securityChampionNominationList = new List<Security_Champion_Nomination__c>([SELECT Id FROM Security_Champion_Nomination__c
                                                                                                                          WHERE Name_of_Security_Champion_Nominee__c =:u.ContactId
                                                                                                                          AND Name_of_Security_Champion_Nominee__r.Attested__c = False
                                                                                                                          WITH SECURITY_ENFORCED]);
        if(!securityChampionNominationList.isEmpty()){
            return null;
        }
        else{
            return Auth.SessionManagement.finishLoginFlow();
        }
    }
    
    public class customData {
        public String Name;
        public String Id;
        public customData(){
            
        }
        public customData(String Name, Id Id){
            this.Name = Name;
            this.Id = Id;
        }
    }
    
}