public class StartAndEndDate{
    @AuraEnabled
    public static StartAndEndDateWrapper getStartAndEndDates(String currentDate){
        StartAndEndDateWrapper startAndEndDateWrapperObj = new StartAndEndDateWrapper(Date.valueOf(currentDate));
        return startAndEndDateWrapperObj;
    }
}

public class StartAndEndDateWrapper {
        @AuraEnabled public Date startDateOfWeek;
        @AuraEnabled public Date endDateOfWeek;
        @AuraEnabled public Date startDateOfMonth;
        @AuraEnabled public Date endDateOfMonth;
        @AuraEnabled public Date startDateOfYear;
        @AuraEnabled public Date endDateOfYear;

        public StartAndEndDateWrapper(){}

       
        public StartAndEndDateWrapper(Date currentDate){
            startDateOfWeek = currentDate.toStartofWeek();
            endDateOfWeek = startDateOfWeek.addDays(6);
            startDateOfMonth = currentDate.toStartOfMonth();
            endDateOfMonth = startDateOfMonth.addDays(Date.daysInMonth(currentDate.year(), currentDate.month())-1);
            startDateOfYear = Date.newInstance(currentDate.year(), 1, 1);
            endDateOfYear = Date.newInstance(currentDate.year(), 12, 31);
        }
}