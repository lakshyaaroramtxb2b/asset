import { LightningElement, track } from 'lwc';


export default class Parent_Component extends LightningElement {
    @track dropDownName = 'Searchable MultiSelect Dropdown';
    @track label = 'Searchable  MultiSelect Dropdown';
    @track value = '';
    @track options = {'Red':'Red',
                      'Blue':'Blue',
                      'Green':'Green',
                      'Yellow':'Yellow'};
    handleFilters(event) {
        let name = event.currentTarget.name;
        let value = event.detail.selection;
        console.log('name = ',name,', selection = ',JSON.stringify(value));
    }

}