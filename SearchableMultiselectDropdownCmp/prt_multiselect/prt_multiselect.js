import { LightningElement, track, api } from 'lwc';

export default class Prt_multiselect extends LightningElement {
    
    @track optionsInternal = [];
    @track selectedOptions = [];
    @track inputPlaceholder = "Select an Option";
    @track selectAllCheckbox = false;
    @track showAll = false;

    isOpen = false;
    handlersAdded = false;
    hasFocus = false;

    @api name;
    @api label = "";
    @api iseditable = false;
    @api emptyMessage = "No Items!"
    // this is to allow the parent compoment to set the option 
    @api
    set options(options) {
        this.optionsInternal = [];
        this.selectedOptions = [];
        options.forEach(option => {
            let internalOption = {
                checked: option.checked ? option.checked : false,
                value: option.value,
                label: option.label
            };
            internalOption.key = this.getHashValue(internalOption);
            this.optionsInternal.push(internalOption);
            if (internalOption.checked) {
                this.selectedOptions.push({
                    value: internalOption.value,
                    label: internalOption.label
                })
            }


        })
        this.showAll = this.optionsInternal.length > 0;
        this.selectAllCheckbox = this.optionsInternal.every(option => option.checked);
        this.updateInputPlaceholder();
    }

    get options() {
        return this.optionsInternal;
    }

    filterOptionInterval = (event) => {
        if (this.iseditable){
            return;
        }
        var filterval = event.currentTarget.value;
        if (filterval == undefined || filterval == null || filterval==''){
            filterval = '';
        }
        this.optionsInternal.map((item)=>{
            if (item.label != undefined && item.label != null && filterval != undefined && filterval != null && item.label.toLowerCase().indexOf(filterval.toLowerCase())>-1){
                item.hideItem = false;
            }
            else{
                item.hideItem = true;
            }
        });
    }
    hideCombobox = event => {
        if (event.currentTarget === event.target) {
            this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
        }
    }
    // handling the input event
    handleInputClicked = event => {
        event.preventDefault();
        this.isOpen = this.template.querySelector('.slds-combobox').classList.toggle('slds-is-open');
    }

    comboboxDropdownClicked = event => {
        event.preventDefault();
    }

    dispatchSelectionEvent = _ => {
        const selectionChangeEvent = new CustomEvent('selectionchange', {
            detail: {
                name: this.name,
                selection: this.selectedOptions,
                allSelected: this.selectAllCheckbox
            }
        });
        this.dispatchEvent(selectionChangeEvent);
    }
    // updating the placehodler value
    updateInputPlaceholder = _ => {
        if (this.selectedOptions.length === 1) {
            this.inputPlaceholder = this.selectedOptions[0].label;
        } else if (this.selectedOptions.length > 1) {
            this.inputPlaceholder = `${this.selectedOptions.length} Options Selected`
        } else {
            this.inputPlaceholder = "Select an Option";
        }
    }
    // handling the select event
    handleSelectClicked = event => {
        let value = event.currentTarget.value;
        this.selectAllCheckbox = true;
        this.selectedOptions = [];
        this.optionsInternal.forEach(option => {
            if (option.value === value) {
                option.checked = !option.checked;
                delete option.key;
                option.key = this.getHashValue(option);
            }
            this.selectAllCheckbox &= option.checked;
            if (option.checked) {
                let { value, label } = option;
                this.selectedOptions.push({
                    value,
                    label
                });
            }
        });
        this.updateInputPlaceholder();
        this.dispatchSelectionEvent();
    }
    // handling the select all event
    handleSelectAll = event => {
        this.selectAllCheckbox = !this.selectAllCheckbox;
        this.selectedOptions = [];
        this.optionsInternal.forEach(option => {
            option.checked = this.selectAllCheckbox;
            delete option.key;
            option.key = this.getHashValue(option);
            if (option.checked) {
                let { value, label } = option;
                this.selectedOptions.push({
                    value,
                    label
                });
            }
        })
        this.updateInputPlaceholder();
        this.dispatchSelectionEvent();
    }
    connectedCallback() {
        this.iseditable = !this.iseditable;
    }
    renderedCallback() {
        if (!this.handlersAdded) {
            this.template.querySelector('.slds-dropdown').addEventListener('focusout', this.hideCombobox)
            this.template.querySelector('.slds-dropdown').addEventListener('focusin', _ => {
                this.hasFocus = true;
            })
            this.template.querySelector('input').addEventListener('blur', event => {
                if (this.isOpen) {
                    this.hasFocus = false;
                    setTimeout(_ => {
                        if (!this.hasFocus) {
                            this.template.querySelector('.slds-combobox').classList.remove(
                                'slds-is-open');
                        }
                    }, 0);
                }
            })
            this.handlersAdded = true;
        }
    }

    getHashValue(obj) {
        if (!obj) {
            return null;
        }
        let parsedString = JSON.stringify(obj);
    
        let hash = 0;
        for (let i = 0; i < parsedString.length; i++) {
            hash = (hash << 5) - hash + parsedString.charCodeAt(i++) | 0;
        }
        return hash + "";
    }
}