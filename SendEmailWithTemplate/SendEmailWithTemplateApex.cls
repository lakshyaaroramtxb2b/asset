public class SF_Utility{
    @AuraEnabled
    public static String sendEmailWithTemplate(String templateName,String subject, String htmlBody,
      List<String> listOfRecepients, List<Id> attachments){
        List<EmailTemplate> listOfEmailTemplates =
        [select id,name,Subject,HtmlValue from EmailTemplate where name =: templateName];
        if(listOfEmailTemplates != null && listOfEmailTemplates.size()>0) {
            EmailTemplate template = listOfEmailTemplates[0];
            string emailFromAddress = System.Label.SF_Email_From_Address;
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: emailFromAddress];
           
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            message.toAddresses = listOfRecepients;
            message.subject = template.Subject.replace('{subject}',subject);
            //get the HTML body from template, replace {body} with the HTML body sent to method as parameter
            message.setHtmlBody(template.HtmlValue.replace('{body}',htmlBody));

            if( !attachments.isEmpty() ){
                message.setEntityAttachments(attachments);
            }
            
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                return 'success';
            }catch(Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
        return 'error';
    }
}