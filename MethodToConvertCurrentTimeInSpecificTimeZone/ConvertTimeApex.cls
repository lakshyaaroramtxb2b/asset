public class ConvertTimeApex{
    public static DateTime convertToCurrentSiteTimezone(DateTime currentDateTime,String timezone){
        String newDateTimeString = currentDateTime.format('yyyy-MM-dd HH:mm:ss',timezone);
        Datetime newDateTime = Datetime.valueofGmt(newDateTimeString);
        return newDateTime;
    }
}