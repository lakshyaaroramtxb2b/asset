public class DateTimeUtility{
    // concert text to time function
    public static Time textToTime( string timeInStr ){
        String[] endPoint = timeInStr.split(':');

        integer sec = 0;
        //Check if contains seconds
        if( endPoint.size() > 2 ){
            sec = Integer.valueof(endPoint[2].subString(0,2));
        }
        return Time.newInstance(Integer.valueof(endPoint[0]), Integer.valueof(endPoint[1]), sec, 00);
    }

    //Get current User Time zone
    public static String getUserTimezone(){
        User currentUser = [Select TimeZoneSidKey from User where id =: USerInfo.getUserId()];
        return currentUser.TimeZoneSidKey;
    }
}  