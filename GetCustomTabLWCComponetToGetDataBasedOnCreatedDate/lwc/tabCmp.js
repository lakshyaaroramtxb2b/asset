import { LightningElement,api,track } from 'lwc';
export default class Sf_task_details_table extends LightningElement {
   
    @track selectedTab = {
        day: false,
        week: true,
        month: false,
        year: false,
        custom: false
    }
    @track tabSelected;
    @track startDate;
    @track startDateWeek;
    @track customRangeStartDate;
    @track customRangeEndDate;
    @track selectedMonth;
    @track selectedYearInMonth;
    @track selectedYear;
    @track yearOptions=[];
    @track currentYear = new Date().getFullYear().toString();
    @track startDateFilter;
    @track endDateFilter;


    connectedCallback(){
        //set Dates accordingly
        this.startDate = selectedDateObj.format('YYYY-MM-DD');
        this.startDateFilter = selectedDateObj.format('YYYY-MM-DD');
        this.startDateWeek = this.startDate;
        this.customRangeStartDate = this.startDate;
        this.customRangeEndDate = this.startDate;
        this.selectedMonth = selectedDateObj.month()+'';
        this.selectedYear = selectedDateObj.year()+'';
        this.selectedYearInMonth = selectedDateObj.year()+'';
    }

    handleTabSelect(event){  
    this.tabSelected = event.target.dataset.tabSelected;
    Object.keys(this.selectedTab).forEach(key => this.selectedTab[key] = false)
    this.selectedTab[this.tabSelected] = true;
    if(this.tabSelected == 'day'){
        this.startDateFilter = this.startDate;
        this.endDateFilter = '';
    }
    else if(this.tabSelected == 'week'){
        this.startDateFilter = this.startDateWeek;
        this.endDateFilter = '';
    }
    else if(this.tabSelected == 'month'){
        var selectedMonthInteger = 1 + parseInt(this.selectedMonth, 10);
        this.startDateFilter = this.selectedYearInMonth+'-'+selectedMonthInteger+'-01';
        this.endDateFilter = '';
    }
    else if(this.tabSelected == 'year'){
        this.startDateFilter = this.selectedYear+'-01-01';
        this.endDateFilter = this.selectedYear+'-12-31';
    }
    else if(this.tabSelected == 'custom'){
        this.startDateFilter = this.customRangeStartDate;
        this.endDateFilter = this.customRangeEndDate;
    }

    //fetchDataFunctionBasedOnStartAndEndDates
   }
}