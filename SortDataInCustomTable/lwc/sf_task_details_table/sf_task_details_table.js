import { LightningElement,api,track } from 'lwc';
import fetchTodoList from '@salesforce/apex/SF_TODO_ListController.fetchTodoList';

export default class Sf_task_details_table extends LightningElement {
    @track columnConfiguration = [];
    @track childColumnConfiguration = [];
    
    @track sortObject;

    


    loadColumns() {
        this.columnConfiguration = [];
        this.columnConfiguration.push({ sortable: false, heading: 'Mark Complete', fieldApiName: 'Mark Complete' });
        this.columnConfiguration.push({ sortable: true, heading: 'Completed By', fieldApiName: 'Completed_By__c' });
        this.columnConfiguration.push({ sortable: false, heading: 'Repeat', fieldApiName: 'Repeat__c' });
        this.columnConfiguration.push({ sortable: true, heading: 'Task Details', fieldApiName: 'Task_Detail__c' });
        this.columnConfiguration.push({ sortable: false, heading: 'Status', fieldApiName: 'Status__c' });
        this.columnConfiguration.push({ sortable: false, heading: 'Assign To', fieldApiName: 'Assign_to__c' });
        this.columnConfiguration.push({ sortable: true, heading: 'Created Date', fieldApiName: 'CreatedDate' });
        this.columnConfiguration.push({ sortable: true, heading: 'Due Date', fieldApiName: 'Due_Date__c' });
    }   

    handleSort = (event) => { 
        let fieldName = event.currentTarget.dataset.fieldname;
        let isValid = false;

        this.columnConfiguration.forEach(column => {
            //check if column is sortable
            if (!column.sortable) {
                return;
            }
            //Find the correct column and update the values for others as well
            if (column.fieldApiName === fieldName) {
                isValid = true;
                column.sorted = true;
                column.sortedAscending = !column.sortedAscending;
                this.sortObject['sortDirection'] = column.sortedAscending ? 'asc' : 'desc';
                this.sortObject['sortfieldName'] = fieldName;
            } else {
                column.sorted = false;
            }
        });
        if (!isValid) {
            return;
        }
        this.fetchTodoData();
    }
    fetchTodoData(){
        this.showSpinner = false;
        var sortfieldName =  this.sortObject.sortfieldName;
        var sortDirection = this.sortObject.sortDirection;
        fetchTodoList({
            startDate : this.startDateFilter,
            endDate : this.endDateFilter,
            tabselect : this.tabSelected,
            status : this.statusFilter,
            taskDetailFilter : this.taskDetailFilter,
            rstMemberId : this.rstMemberFilterId,
            sortFieldName : sortfieldName,
            sortDirection : sortDirection,
            showAllTask : this.showAllTask
        })
        .then(result => {
            this.todoData = result;
            if(this.todoData.length > 0){
                this.noTodoData = false;
                for(var i=0;i<this.todoData.length;i++){
                    this.todoData[i].ismodifyparent = false;
                    if(this.todoData[i].assignToName == '' || this.todoData[i].assignToName == null){
                        this.todoData[i].teamName = this.currentSiteName + ' Team';
                    }
                    var availableDate = null;
                    if(this.todoData[i].repeat === 'Weekly'){
                         availableDate = moment().tz(this.timeZone).endOf('week').format('YYYY-MM-DD');
                    }else if(this.todoData[i].repeat === 'Monthly'){
                         availableDate = moment().tz(this.timeZone).endOf('month').format('YYYY-MM-DD');
                    }else if(this.todoData[i].repeat === 'Yearly'){
                         availableDate = moment().tz(this.timeZone).endOf('year').format('YYYY-MM-DD');
                    }
                    if(availableDate != null && (this.todoData[i].createdDate > availableDate)){ // if(this.todoData[i].createdDate > this.todayDate){
                        this.todoData[i].isFutureTask = true;
                        this.todoData[i].content = 'This is a  '+this.todoData[i].repeat +' task, it will be available after '+ availableDate;
                    }
                    if(this.todoData[i].status == 'Completed'){
                        this.todoData[i].statusjs  = 'Completed';  
                        this.todoData[i].showEditButton = false;
                        this.todoData[i].check = true;
                        this.todoData[i].labellinethrough = 'labellinethrough';
                    }
                    else{
                        if(this.todoData[i].dueDate >= this.todayDate || this.todoData[i].dueDate == null || this.todoData[i].dueDate == undefined || this.todoData[i].dueDate == ''){
                          this.todoData[i].statusjs  = 'Pending';  
                        }
                        else{
                            this.todoData[i].statusjs  = 'Overdue';  
                        }
                        this.todoData[i].showEditButton = true;
                        this.todoData[i].check = false;
                        this.todoData[i].labellinethrough='';
                    }
                    if( this.todoData[i].dueDate == null || this.todoData[i].dueDate == undefined || this.todoData[i].dueDate == ''){
                        this.todoData[i].showHideShowButton = true;
                    }else{
                        this.todoData[i].showHideShowButton = false; 
                    }
                }
                this.showSpinner = false;
            }
            else{
                this.noTodoData = true;
                this.showSpinner = false;
            }
        })
        .catch(error=>{
            this.showSpinner = false;
            let message = error.message || error.body.message;
            showMessage(this, { message: message, messageType: 'error', mode: 'pester' });
        })
    }
    
    connectedCallback(){
        this.sortObject = {};
        this.sortObject['sortDirection'] = '';
        this.sortObject['sortfieldName'] = '';
        this.loadColumns();
    }

   
}