public without sharing class SF_Apex_Sort {
  

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method sort data in ASC/DESC Order on apex side.
    */
   @AuraEnabled
   public static List<ParentWrapper> sortDataBasedOnDirection(String sortFieldName,String sortDirection,List<Object_Api_Name> dataToSort ){
       List<ParentWrapper> parentWrapperList = new List<ParentWrapper>();
       if(sortDirection != '' && sortFieldName != ''){
        Map<String,List<Object_Api_Name>> fieldToDataMap= new Map<String,List<Object_Api_Name>>();
        for(Object_Api_Name tempObj : dataToSort){
            if(sortFieldName == 'Completed_By__c'){
                if(!fieldToDataMap.containsKey(tempObj.Completed_By__c)){
                    fieldToDataMap.put(tempObj.Completed_By__c,new List<Object_Api_Name>());
                }
                fieldToDataMap.get(tempObj.Completed_By__c).add(tempObj);
            }
            else if(sortFieldName == 'Task_Detail__c'){
                if(!fieldToDataMap.containsKey(tempObj.Task_Detail__c)){
                    fieldToDataMap.put(tempObj.Task_Detail__c,new List<Object_Api_Name>());
                }
                fieldToDataMap.get(tempObj.Task_Detail__c).add(tempObj);
            }
            else if(sortFieldName == 'Due_Date__c'){
                if(!fieldToDataMap.containsKey(tempObj.Due_Date__c+'')){
                    fieldToDataMap.put(tempObj.Due_Date__c+'',new List<Object_Api_Name>());
                }
                fieldToDataMap.get(tempObj.Due_Date__c+'').add(tempObj);
            }else if(sortFieldName == 'CreatedDate'){
                if(!fieldToDataMap.containsKey(tempObj.Entry_Date__c+'')){
                    fieldToDataMap.put(tempObj.Entry_Date__c+'',new List<Object_Api_Name>());
                }
                fieldToDataMap.get(tempObj.Entry_Date__c+'').add(tempObj);
            }
            else if(sortFieldName == 'CreatedDate'){
                if(!fieldToDataMap.containsKey(tempObj.Entry_Date__c+'')){
                    fieldToDataMap.put(tempObj.Entry_Date__c+'',new List<Object_Api_Name>());
                }
                fieldToDataMap.get(tempObj.Entry_Date__c+'').add(tempObj);
            }
        }

        List<String> sortOrderList = new List<String>();
        sortOrderList.addAll(fieldToDataMap.keySet());
        sortOrderList.sort();
        if(sortDirection == 'desc'){
            List<String> reverseOrderList = new List<String>();
            for(Integer i= sortOrderList.size()-1; i>=0;i--){
                reverseOrderList.add(sortOrderList[i]);
            }
            sortOrderList = reverseOrderList;
        }

      

        for(String key :  sortOrderList){
            for(Object_Api_Name tempObj : fieldToDataMap.get(key)){
                ParentWrapper wrapperObj = new ParentWrapper(tempObj);
                //For fetching Child Object data
                for(Object_Api_Name childObj : tempObj.Object_Api_Name__r){
                    wrapperObj.childWrapperList.add(new ChildWrapper(childObj));
                }  
                parentWrapperList.add(wrapperObj);
            }
        }
      
       }
       if(parentWrapperList.isEmpty()){
           for(Object_Api_Name tempObj : dataToSort){
            ParentWrapper wrapperObj = new ParentWrapper(tempObj);  
                for(Object_Api_Name childObj : tempObj.Object_Api_Name__r){
                    wrapperObj.childTodoWrapperList.add(new ChildWrapper(childObj));
                }  
            parentWrapperList.add(wrapperObj);
           }
       }
       
       return parentWrapperList;
   }

   
}