public  with sharing class DML {
    public static SObject doInsert( SObject sObj ) {
        if ( !sObj.getSObjectType().getDescribe().isCreateable() ) {
            throw new SF_Exception('Insufficient create access. ( Object: '+sObj.getSObjectType() +' )' );
        }
            
        insert sObj;
        return sObj;
    }
}

public static List<SObject> doInsert( List<SObject> sObjs ) {
        if ( !sObjs.isEmpty() && !sObjs[0].getSObjectType().getDescribe().isCreateable() ) {
            throw new SF_Exception('Insufficient create access. ( Object: '+sObjs[0].getSObjectType() +' )' );
        }

        insert sObjs;
        return sObjs;
}